require 'test_helper'

class ExchangeRateTest < ActionDispatch::IntegrationTest
  test 'should get dates' do

    exchange_rate = ExchangeRate.new

    assert exchange_rate.get_available_dates.present?
  end

  test 'should get currencies' do
    exchange_rate = ExchangeRate.new

    assert exchange_rate.get_available_currencies.present?
  end

  test 'should find currency' do
    exchange_rate = ExchangeRate.new

    assert exchange_rate.find_currency('EUR').present?
  end

  test 'should get rate' do
    exchange_rate = ExchangeRate.new

    assert exchange_rate.at('2017-05-28', 'EUR', 'GBP').present?
  end

  test 'is a float' do
    exchange_rate = ExchangeRate.new

    assert_equal true, exchange_rate.is_float('100.123')
  end

  test 'is not a float' do
    exchange_rate = ExchangeRate.new

    assert_equal false, exchange_rate.is_float('100,000')
  end

  test 'calculation of float matches' do
    exchange_rate = ExchangeRate.new

    assert_equal 150.1845, exchange_rate.calculate(Rate.second, '100.123')
  end

  test 'calculation of integer matches' do
    exchange_rate = ExchangeRate.new

    assert_equal 150, exchange_rate.calculate(Rate.second, '100')
  end

  test 'data source is class' do
    assert ENV['EXCHANGE_RATE_DATA_SOURCE'].constantize.new.present?
  end

  test 'data source has method' do
    data_source = ENV['EXCHANGE_RATE_DATA_SOURCE'].constantize.new

    assert data_source.respond_to?(:update_data)
  end
end
