require 'test_helper'
require 'nokogiri'
require 'open-uri'

class ExchangeRate::Parsers::EcbParserTest < ActionDispatch::IntegrationTest
  test 'should get doc' do
    doc = Nokogiri::HTML(open('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml'))

    assert doc.present?
  end

  test 'should get date nodes' do
    doc = Nokogiri::HTML(open('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml'))
    parser = ExchangeRate::Parsers::EcbParser.new

    assert parser.get_date_nodes(doc).present?
  end

  test 'should get currency nodes' do
    doc = Nokogiri::HTML(open('http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml'))
    parser = ExchangeRate::Parsers::EcbParser.new

    date = parser.get_date_nodes(doc).first

    assert parser.get_currency_nodes(date).present?
  end

  test 'should get currency name' do
    parser = ExchangeRate::Parsers::EcbParser.new

    assert_equal 'Euro', parser.get_currency_name('EUR')
  end
end
