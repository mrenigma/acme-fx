require_relative 'boot'

require 'rails/all'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module AcmeFx
  class Application < Rails::Application
    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Autoload the lib dir
    config.autoload_paths << Rails.root.join('lib')
    # Add node modules to assets path
    config.assets.paths << Rails.root.join('node_modules')

    # Enable browserify
    config.browserify_rails.force = true
    config.browserify_rails.commandline_options = [
        '--extension ".es6"',
        '-t [ babelify ]'
    ]
    # Enable sourcemaps in dev
    config.browserify_rails.source_map_environments << 'development'
    # Limit browserify folders
    config.browserify_rails.paths = [
        lambda {|p| p.start_with?(Rails.root.join('node_modules').to_s)},
        lambda {|p| p.start_with?(Rails.root.join('app/assets/javascripts').to_s)}
    ]
  end
end
