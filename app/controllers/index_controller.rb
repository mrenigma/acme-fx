class IndexController < ApplicationController
  def index
    exchange_rate = ExchangeRate.new

    # Get a list of dates for select input
    @dates = exchange_rate.get_available_dates
    # Get a list of currencies for select input
    @currencies = exchange_rate.get_available_currencies

    # If the date isn't set, default it to the latest date retrieved from the data source
    if params['date'].blank?
      params['date'] = @dates.first.first
    end

    # If the GET params have been set calculate the value after conversion
    if params['date'].present? and params['from_currency'].present? and params['to_currency'].present?
      rate = exchange_rate.at(params['date'], params['from_currency'], params['to_currency'])

      # Set a default amount of 1 if no amount is defined
      if params['amount'].blank?
        params['amount'] = '1'
      end

      if rate.present?
        @value = exchange_rate.calculate(rate, params['amount'])
      end
    end
  end
end
