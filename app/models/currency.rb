class Currency < ApplicationRecord
  has_many :associated_rates, :class_name => 'Rate'
  has_many :rates, :class_name => 'Rate'
end
