class Rate < ApplicationRecord
  belongs_to :base, :class_name => 'Currency'
  belongs_to :currency, :class_name => 'Currency'
end
