/* global $, jQuery */
import CurrencyAutoComplete from './modules/currency-autocomplete';
import DateSelect from './modules/date-select';

(function ($) {
    "use strict";

    const debug = true;

    /**
     * Currency auto complete
     */
    const CurrencyAutoCompleteClass = new CurrencyAutoComplete('.js-currency-autocomplete');

    /**
     * Date select
     */
    const DateSelectClass = new DateSelect('.js-date-select');
})($);
