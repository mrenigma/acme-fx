class CurrencyAutoComplete {
    constructor (selector) {
        this.$inputs = $(selector);

        this.$inputs.autocomplete({
            source : this.$inputs.data('src')
        });
    }
}

export default CurrencyAutoComplete;
