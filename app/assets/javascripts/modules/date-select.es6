import empty from './utils/empty';

class DateSelect {
    constructor (selector) {
        this.$inputs = $(selector);

        this.$inputs.on('change', $.proxy(this.checked, this)).trigger('change');
    }

    checked (e) {
        const $elem = $(e.currentTarget);

        if (empty($elem.val())) {
            $elem.removeClass('has-date');
        } else {
            $elem.addClass('has-date');
        }
    }
}

export default DateSelect;
