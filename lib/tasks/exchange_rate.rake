namespace :exchange_rate do
  desc 'Update data from the data source to your DB'

  task :update_data, [:silent] => :environment do |task, args|
    exchange_rate = ExchangeRate.new
    updated = exchange_rate.update_data

    args.with_defaults(:silent => false)

    if args.silent.blank?
      if updated
        puts 'Sync completed'
      else
        puts 'Sync failed'
      end
    end
  end

end
