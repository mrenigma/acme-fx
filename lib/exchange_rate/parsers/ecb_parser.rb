require 'nokogiri'
require 'open-uri'

class ExchangeRate::Parsers::EcbParser
  # Data source URL
  URL = 'http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml'
  # Base currency code
  BASE = 'EUR'

  # Get all the date nodes from the XML
  def get_date_nodes (doc)
    doc.xpath('//cube//cube[@time]')
  end

  # Get all the currency nodes from the XML
  def get_currency_nodes (doc)
    doc.xpath('.//cube[@currency]')
  end

  # Using an free-to-use list of currency codes get the currency name
  def get_currency_name(currency_code)
    currencies_doc = Nokogiri::HTML(open('https://www.currency-iso.org/dam/downloads/lists/list_one.xml'))

    currencies_doc.xpath('//ccytbl//ccyntry/ccy').each do |currency_code_node|
      if currency_code_node.text == currency_code
        return currency_code_node.parent.xpath('ccynm').text
      end
    end

    ''
  end

  # Find or create the base currency based off the BASE variable
  def find_or_create_base_currency
    currency = Currency.find_or_create_by(currency_code: BASE)

    if currency.currency_name.blank?
      currency.currency_name = self.get_currency_name(BASE)
      currency.save!
    end

    currency
  end

  # Find or create the currency based off the passed in currency code
  def find_or_create_currency(currency_code)
    currency = Currency.find_or_create_by(currency_code: currency_code)

    if currency.currency_name.blank?
      currency.currency_name = self.get_currency_name(currency_code)
      currency.save!
    end

    currency
  end

  # Create a new rate if it doesn't already exist (captures repeated update_date calls)
  def create_rate(date, currency, rate_value)

    # Get the number of decimal places to do a consistent comparison in the query
    decimal_places = rate_value.to_s.split('.')[1].length
    base = self.find_or_create_base_currency

    rate = Rate.find_by_sql(['SELECT `rates`.* FROM `rates` INNER JOIN `currencies` ON `currencies`.`id` = `rates`.`currency_id` INNER JOIN `currencies` AS `bases` ON `bases`.`id` = `rates`.`base_id` WHERE `rates`.`date` = ? AND FORMAT(`rates`.`rate`, ?) = FORMAT(?, ?) AND `currencies`.`id` = ? AND `bases`.`id` = ?', date, decimal_places, rate_value, decimal_places, currency.id, base.id]).first

    if rate.present?
      return rate
    end

    rate = Rate.new
    rate.base = base
    rate.date = date
    rate.rate = rate_value
    rate.currency = currency
    rate.save!

    rate
  end

  # Get the data from the data source and then build the currencies and rates
  def update_data
    doc = Nokogiri::HTML(open(URL))

    self.get_date_nodes(doc).each do |date_node|
      date = Date::strptime(date_node.attribute('time').to_str, '%Y-%m-%d')

      self.get_currency_nodes(date_node).each do |currency_node|
        currency_code = currency_node.attribute('currency').to_str
        rate_value = currency_node.attribute('rate').to_str.to_f

        currency = self.find_or_create_currency(currency_code)
        rate = self.create_rate(date, currency, rate_value)

        if currency.blank? or rate.blank?
          return false
        end
      end
    end

    true
  end
end
