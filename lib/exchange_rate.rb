class ExchangeRate
  # Get list of available dates for a select input
  def get_available_dates
    dates = []

    Rate.order('date DESC').all.distinct.pluck('date').each do |date|
      dates.push([date.to_s, date.to_formatted_s(:exchange_date)])
    end

    dates
  end

  # Get list of available currencies for a select input
  def get_available_currencies
    currencies = []

    Currency.find_each do |currency|
      currencies.push({label: currency.currency_code + ' - ' + currency.currency_name, value: currency.currency_code})
    end

    currencies
  end

  # Find currency by currency code
  def find_currency(currency_code)
    Currency.find_by_currency_code(currency_code)
  end

  # Get the rate on a specified date for the conversion currencies
  def at(date, from, to)
    Rate.includes('currency', 'base').where({date: date, base: find_currency(from).id, currency: find_currency(to).id}).first
  end

  # Check if a string is a float
  def is_float(str)
    str.to_s.split('.').length > 1
  end

  # Multiple the amount given by the rates rate
  def calculate(rate, amount)
    if is_float(amount)
      amount = Float(amount)
    else
      amount = Integer(amount, 10)
    end

    (amount * rate.rate)
  end

  # Run the update_data method for the data source
  def update_data
    data_source = ENV['EXCHANGE_RATE_DATA_SOURCE'].constantize.new

    data_source.update_data
  end
end
