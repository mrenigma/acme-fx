# The challenge

Your challenge for today is to delve deep into the work of foreign exchange (FX) to provide a
**Ruby library** for obtaining FX rates and a **web interface**​ to demo the functionality of the library.

## Interface

Here’s how a client would expect to use the library where GBP is the base currency and USD is
the counter currency:  

ExchangeRate.at(Date.today,'GBP','USD')

## Data Source
During development, the data source of FX rates will be the 90day European Central Bank
(ECB) feed: http://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml

It’s likely we’ll use an alternative provider in production.

## Data Storage
We don’t want to be dependant on the ECB website in order for our library to work, so the data
should be prefetched and stored locally. Your solution should provide a mechanism to be used
by a scheduler like cron to retrieve data once a day.

When requesting a rate, the library should load it from the store, not from the feed.

## Web App
To demonstrate the library, we would like you to provide a Ruby based web app that
demonstrates converting between the available currencies for a given date, defaulting to today.

Here’s how it might look:
![Mockup of app](https://gitlab.com/mrenigma/acme-fx/uploads/b475f7b39abe2924258efc942d8755a3/A9Rbfbf98_j9j3kz_b7g.jpg)

## Implementation
Please implement your solution in Ruby. You can chose whatever additional frameworks, gems
and technologies best fit your solution.

## Time Scales
Please take as long as you need to produce a solution you are proud of. The majority of
candidates submit a solution within two weeks. If you need longer due to other commitments
that’s ok - just check in with us regularly so we know you’re still interested in the role.

## Delivery
We would prefer you to deliver your solution as a zip file. If that presents a problem for any
reason, just let us know.